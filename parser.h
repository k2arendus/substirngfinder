// parser.h
#ifndef PARSER_H 
#define PARSER_H

#include <string>
#include <map>

typedef std::map<std::string, int> subStringMap;
int subString(const std::string& str, int lineLenght, subStringMap& strings);
void findTopStrings(subStringMap& subStringMap, int totalStrings);
void plotMultimap(subStringMap& subStringMap, int totalStrings);

//#define DEBUG

#endif 