/*
 * parser.centryentry
 * 
 * Copyright 2018 k2 <k2@k2pc>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#// ifstream constructor.
#include <iostream>     
#include <fstream>
#include <bits/stdc++.h> 
#include <string>
#include <map>
#include <set>
#include <algorithm>
#include <functional>
#include <vector>
#include "parser.h"

//#define DEBUG

using namespace std;

template <typename A, typename B>
multimap<B, A> flipMap(map<A,B> & src) {

    multimap<B,A> dst;

    for(typename map<A, B>::const_iterator it = src.begin(); it != src.end(); ++it)
        dst.insert(pair<B, A>(it -> second, it -> first));

    return dst;
}

int main(int argc, char* argv[])
{
  if(argc != 2){
    cout << "too many or too few arguments" << endl;
    exit(1);
  }
  string filename = argv[1];
  ifstream inputFile;
  string line;
  subStringMap subStringMap;
  int totalStrings = 0;


  inputFile.open(filename);
  if(!inputFile){
    cerr << "Unable to open file ";
    exit(1);
  }
  //Read all lines and store all strings in map
  while (!inputFile.eof()) {
    getline(inputFile,line);
    int lineLenght = line.length();  
    totalStrings = totalStrings + subString(line,lineLenght,subStringMap);

  }
  //find top 10 strings with highest occurances
  findTopStrings(subStringMap,totalStrings);
  inputFile.close();
  return 0;
}

/**
 * @brief Takes in a string, lenght of the string and a map where string keys are mapped to their occurances.
 * Generates all substrings from an input string longer than 3 characters and inserts them to map and increments
 * occurance value. 
 * 
 * @param str - string where all substrings are to be eentrytracted from
 * @param lineLenght - lenght of the passed string
 * @param strings - map containing strings and occurances <string, nr of occurances>
 * @return int - returns number of substrings generated 
 */
int subString(const std::string& str, int lineLenght, subStringMap& strings)  
{ 
  
  int wordcount = 0;
    // Pick starting point. Can't be closer than four chars from EOL
    for (int start = 0; start <= lineLenght - 4; start++)  
    {
      //Check if first four characters don't contain space - string is at least 4 characters
      if(str[start] != ' ' && str[start+1] != ' ' && str[start+2] != ' ' && str[start+3] != ' '){
        //Pick end point (at least four chars away from start point)
        for (int end = start + 3; end <= lineLenght - 1 && str[end] != ' '; end++)
        { 
          ++strings[str.substr(start,end-start+1)];
          wordcount++;  
#ifdef DEBUG
          std::cout <<"start: " <<  start << " end: " << end << " ";
          for (int k = start; k <= end; k++){
            std::cout << str[k];
          }
          std::cout << std::endl;
#endif        
        } 
      } 
        
    } 
    return wordcount; 
}
/**
 * @brief Takes a map with strings as keys and occurance of those strings as values and generated a submap
 * containing top 10 occuring strings. Results are printed to console
 * 
 * @param stringMap - map containing strings with occurances <string, int>
 * @param totalStrings - number of total strings in map
 */
void findTopStrings(subStringMap& stringMap, int totalStrings){

  subStringMap topStrings; //map of strings to be printed
  const int printCount = 10; //number of strings to be printed
  int mapEntryCount = 0; //number of strings entered to map to be printed
  pair <string,int> lowestOccuranceEntry; //string with lowest occurance that is entered to the map to be printed

  //start populating the map to be printed
  for(auto& entry: stringMap){

    //first $printcount strings get entered automatically. Track is only kept of the entry with lowest occurance.
    if(mapEntryCount < printCount){
      if(lowestOccuranceEntry.second < entry.second){
        lowestOccuranceEntry.second = entry.second;
        lowestOccuranceEntry.first = entry.first;
      }
      topStrings.insert (std::pair<string,int>(entry.first, entry.second));
      mapEntryCount++;
    //In case there are more than $printcount entries in the map then each potential entry is compared with current lowest entry
    }else if(lowestOccuranceEntry.second < entry.second){

      //erase lowest occuring entry and insert new
      topStrings.erase(lowestOccuranceEntry.first);
      topStrings.insert (std::pair<string,int>(entry.first, entry.second));
    
      //set last entry as lowest entry with lowest occurance by default
      lowestOccuranceEntry.second = entry.second;
      lowestOccuranceEntry.first = entry.first;

      //check if there are lower occuring strings in topList than last inserted string
      for(auto& topstring: topStrings){
        if(lowestOccuranceEntry.second > topstring.second){
          lowestOccuranceEntry.first = topstring.first;
          lowestOccuranceEntry.second = topstring.second;
        }
      }
    }
    
  }
  plotMultimap(topStrings,totalStrings);
}

/**
 * @brief Takes a map of strings with occurances as values and number of total strings in text.
 * Orders strings form highest occurance and prints results to console. Adds bar plot.
 * 
 * @param subStringMap - map of strings to be ordered <string, (int)nr of occurances>
 * @param totalStrings - total number of substrings in text
 */
void plotMultimap(subStringMap& subStringMap, int totalStrings){

  cout << "Number of substrings in text: " << totalStrings << endl;
  multimap<int, string> reverseTest = flipMap(subStringMap);

  cout << endl <<setw(10) << "word" << "  " << setw(10) << "frequency" << endl;
  for(int i = 0; i < 6; i++){
    cout << "----------------------";
  }
  cout << endl;

  for(multimap<int, string>::const_reverse_iterator it = reverseTest.rbegin(); it != reverseTest.rend(); ++it){
    float freqs = (float)it->first/(float)totalStrings * 100;
    cout.precision(4);
    cout << setw(10);
    cout << it->second << ": "<< setw(9) << freqs << '%' << " | ";
    for(int i = 0; i < 100 - 6; i++){
      if(i < int(freqs)){
        cout << '*';
      }else if(i - 1 < int(freqs)){
        cout << freqs << '%';
      }else{
        cout << ' ';
      }
      
    }
    cout << "| 100% "<<endl;
  }
}


