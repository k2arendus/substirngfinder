Substring counter
===

Program to count all more than 3 character long substrings in a text and find most common substrings.

#### Tested with gcc 7.3.0


### Building

````
g++ parser.cxx
````

### Running

````
./a.out <filename>
````

#### Todo
 
- Replace map with structure with one that makes use of ovelapping strings
- Checks for file string input
- Bar plot with dynamic scale 